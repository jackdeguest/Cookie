$ENV{ 'APACHE_LOCK_DIR' } = q|/var/lock/apache2|;
$ENV{ 'APACHE_LOG_DIR' } = q|/var/log/apache2|;
$ENV{ 'APACHE_PID_FILE' } = q|/var/run/apache2/apache2.pid|;
$ENV{ 'APACHE_RUN_DIR' } = q|/var/run/apache2|;
$ENV{ 'APACHE_RUN_GROUP' } = q|www-data|;
$ENV{ 'APACHE_RUN_USER' } = q|www-data|;
$ENV{ 'HAS_APACHE_TEST' } = q|1|;
$ENV{ 'HAS_SSL' } = q|1|;

1;
